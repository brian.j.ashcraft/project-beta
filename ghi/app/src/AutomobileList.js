import React, { Component } from "react";

function AutomobileTable(props) {
  return (
    <tr key={props.automobile.id}>
      <td>{props.automobile.vin}</td>
      <td>{props.automobile.color}</td>
      <td>{props.automobile.year}</td>
      <td>{props.automobile.model.name}</td>
      <td>{props.automobile.model.manufacturer.name}</td>
    </tr>
  );
}

class AutomobileList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      automobiles: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    const data = await response.json();

    if (response.ok) {
      const transformedData = data.autos.map((auto) => ({
        id: auto.id,
        vin: auto.vin,
        color: auto.color,
        year: auto.year,
        model: {
          id: auto.model.id,
          name: auto.model.name,
          manufacturer: {
            name: auto.model.manufacturer.name,
          },
        },
      }));

      this.setState({ automobiles: transformedData });
    }
  }

  render() {
    return (
      <>
        <div>
        <h1 className="display-4 fw-bold text-center" style={{ color: "#0047ab", fontFamily: "Playfair Display, serif" }}>
            Automobiles
          </h1>
          <table className="table table-striped exotic-table">
            <thead>
              <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
              </tr>
            </thead>
            <tbody>
              {this.state.automobiles.map((automobile) => {
                return <AutomobileTable automobile={automobile} key={automobile.id} />;
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default AutomobileList;
