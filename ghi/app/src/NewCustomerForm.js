import React, { useState, useEffect } from 'react';

function CreateCustomer() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState(' ')

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.address = address
        data.phone_number = phoneNumber

        const customerURL = "http://localhost:8090/api/customer/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(customerURL, fetchConfig)
        if (response.ok) {
            const newCustomer = await response.json()
            console.log(newCustomer)
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhoneNumber(' ')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
            <h2>Create Customer</h2>
            <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="First name" className="form-control" id="first_name" name="first_name" value={firstName} onChange={handleFirstNameChange} />
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Last Name" className="form-control" id="last_name" name="last_name" value={lastName} onChange={handleLastNameChange} />
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Address" className="form-control" id="address" name="address" value={address} onChange={handleAddressChange} />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Phone Number" className="form-control" id="phone_number" name="phone_number" value={phoneNumber} onChange={handlePhoneNumberChange} />
                    <label htmlFor="first_name">Phone Number</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    );
}

export default CreateCustomer;

