import React, { useEffect, useState } from "react";

function ModelsList() {
  const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchModels = async () => {
      const response = await fetch("http://localhost:8100/api/models/");
      const data = await response.json();
      setModels(data.models);
    };

    fetchModels();
  }, []);

  return (
    <>
      <div className="container">
        <div className="models-list">
        <h1 className="display-4 fw-bold text-center" style={{ color: "#0047ab", fontFamily: "Playfair Display, serif" }}>Models</h1>
          <table className="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {models.map((model) => (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td>
                  <img src={model.picture_url} alt="car" height="110" />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default ModelsList;
