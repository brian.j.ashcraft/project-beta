import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentList from './AppointmentList';
import AppointmentsForm from "./AppointmentsForm";
import AutomobileForm from "./AutomobileForm";
import AutomobileList from "./AutomobileList";
import CustomerList from "./CustomerList";
import ManufacturersForm from "./ManufacturersForm";
import ManufacturersList from "./ManufacturersList";
import CreateCustomer from "./NewCustomerForm";
import CreateSale from "./SaleForm";
import SalesList from "./SalesList";
import CreateSalesperson from "./NewSalespersonForm";
import SalespersonList from "./SalespersonList";
import SalesHistory from "./SalesHistory";
// import ServiceForm from "./ServiceForm";
import ModelForm from "./ModelForm";
import ModelList from "./ModelList";
import ServiceHistory from "./ServiceHistory";
import SaleHistory from "./SalesList";



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/new" element={<AppointmentsForm />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturers/new" element={<ManufacturersForm />} />
          <Route path="/customer/new" element={<CreateCustomer />} />
          <Route path="/customer" element={<CustomerList />} />
          <Route path="/salespeople" element={<SalespersonList />} />
          <Route path="/salespeople/new" element={<CreateSalesperson />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/new" element={<CreateSale />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobile" element={<AutomobileForm />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="/model" element={<ModelForm />} />
          <Route path="/service/history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
