import React from 'react';
import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 text-wrap">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdownInventory"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownInventory">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/new">
                    Create a Manufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers">
                    Manufacturers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/model">
                    Create a Model
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/models">
                    Models
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobile">
                    Create an Automobile
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdownSales"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/new">
                    Add a Salesperson
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople">
                    Salespeople
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customer/new">
                    Add a Customer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customer">
                    Customers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/new">
                    Add a Sale
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales">
                    Sales
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/history">
                    Salesperson History
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdownService"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownService">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/new">
                    Add a Technician
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians">
                    Technicians
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/new">
                    Create a Service Appointment
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments">
                    Service Appointments
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/service/history">
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
