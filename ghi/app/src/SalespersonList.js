import React, { useState, useEffect } from 'react';

function SalespersonList() {
  const [salespersons, setSalesperson] = useState([]);



  useEffect(() => {
    const fetchSalesperson = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (!response.ok) {
          throw new Error("ALERT ALERT ALERT!");
        }
        const data = await response.json();
        setSalesperson(data.salespersons)
        console.log(data.salespersons)
    } catch (error) {
        console.error(error);
    }
    };
    fetchSalesperson();
  }, []);


  return (
    <>
      <h1>Salespersons</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespersons.map((each) => (
            <tr key={each.id}>
              <td>{each.employee_id}</td>
              <td>{each.first_name}</td>
              <td>{each.last_name}</td>           
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default SalespersonList;

