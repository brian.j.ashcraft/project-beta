import React, { useState, useEffect } from 'react';

function SaleList() {
    const [sales, setSale] = useState([]);

    useEffect(() => {
        const fetchSale = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/sales/history');
                if (!response.ok) {
                    throw new Error("Error");
                }
                const data = await response.json();
                setSale(data.sales)
                console.log(data.sales)
            } catch (error) {
                console.error(error);
            }
        };
        fetchSale();
    }, []);

    return (
        <>
            <h1>Salesperson History</h1>
            <div className="mb-3"> Salesperson
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((each) => (
                        <tr key={each.id}>
                            <td>{each.salesperson.first_name} {each.salesperson.last_name} </td>
                            <td>{each.customer.first_name} {each.customer.last_name}</td>
                            <td>{each.automobile.vin}</td>
                            <td>{each.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default SaleList;