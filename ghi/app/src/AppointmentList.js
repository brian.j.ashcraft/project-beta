import React, { useState, useEffect } from 'react';

export function AppointmentTable(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th scope="col">VIN</th>
          <th scope="col">Customer</th>
          <th scope="col">Date</th>
          <th scope="col">Time</th>
          <th scope="col">Technician</th>
          <th scope="col">Reason</th>
          {!props.hideVip && <th scope="col">VIP</th>}
          {props.showStatus && <th scope="col">Status</th>}
        </tr>
      </thead>
      <tbody>
        {props.appointments.map((appointment) => {
          return (
            <appointment
              hideActions={props.hideActions}
              hideVip={props.hideVip}
              showStatus={props.showStatus}
              appointment={appointment}
              key={appointment.id}
              cancel={props.cancelAppointment}
              finish={props.finishAppointment}
            />
          );
        })}
      </tbody>
    </table>
  );
}

function AppointmentList(){
  const [appointments, setAppointment] = useState([]);

  async function fetchAppointment(){
    const res = await fetch('http://localhost:8080/api/appointments');
    const newAppointment = await res.json();
    setAppointment(newAppointment.appointments.filter(record => record.status !== "finished"))
  }

    useEffect(()=> {
      fetchAppointment()
    }, [])

  const cancel = async (e, id) => {
    const url =`http://localhost:8080/api/appointments/${id}/`
    const fetchConfig = {
      method: 'PUT',
      body: JSON.stringify({status:3}),
      headers: {'Content-Type': 'application/json',}
      
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      fetchAppointment()
    }

  }

  const finish = async (id) => {
    const url =`http://localhost:8080/api/appointments/${id}/`
    const fetchConfig = {
      method: 'PUT',
      body: JSON.stringify({status:2}),
      headers: {'Content-Type': 'application/json',}
      
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      fetchAppointment()
    }
  }

  return (
    <>
      <h1 className="display-4 fw-bold text-center" style={{ color: "#0047ab", fontFamily: "Playfair Display, serif" }}>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer && appointment.customer.name}</td>
              <td>{appointment.date_time.split("T")[0]}</td>
              <td>{appointment.date_time.split("T")[1].slice(0, 5)}</td>
              <td>{appointment.technician ? appointment.technician.id : ''}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.vip ? "Yes" : "No"}</td>
              <td>{appointment.status}</td>
              <td>
                <button
                  onClick={(e) => cancel(e, appointment.id)}
                  type="button"
                  className="btn btn-danger rounded-0"
                >
                  Cancel
                </button>
                <button
                  onClick={() => finish(appointment.id)}
                  type="button"
                  className="btn btn-success rounded-0"
                >
                  Finished
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default AppointmentList;
