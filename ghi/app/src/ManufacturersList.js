import React, { useState, useEffect } from 'react';
import Alert from './Alert';

const ManufacturerList = () => {
  const [manufacturers, setManufacturers] = useState([]);
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      if (!response.ok) {
        throw new Error('Manufacturers response not successful!');
      }
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } catch (error) {
      setAlert(true);
      setAlertMessage('Error fetching the manufacturers');
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1>Manufacturers</h1>
      {alert && <Alert alert={alert} message={alertMessage} />}
      <div className="table-responsive">
        <table className="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map((manufacturer) => (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ManufacturerList;
