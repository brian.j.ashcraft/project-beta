import React, { useState, useEffect } from 'react';
import Alert from './Alert';


const TechnicianList = () => {
  const [technicians, setTechnicians] = useState([]);
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/technicians');
      if (!response.ok) {
        throw new Error('Technicians response not successful!');
      }
      const data = await response.json();
      setTechnicians(data.technicians);
    } catch (error) {
      setAlert(true);
      setAlertMessage('Error fetching the technicians');
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (employee_id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/technicians/${employee_id}`, {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error('There was a problem with the deletion.');
      }
      setAlert(true);
      setAlertMessage('Deletion successful!');
      setTechnicians((prevTechnicians) =>
      prevTechnicians.filter((technician) => technician.employee_id !== employee_id)
      );
    } catch (error) {
      setAlert(true);
      setAlertMessage('There was a problem with the deletion.');
    }
  };

  return (
    <>
      <h1 className="display-4 fw-bold text-center" style={{ color: "#0047ab", fontFamily: "Playfair Display, serif" }}>Technicians</h1>
      {alert && <Alert alert={alert} message={alertMessage} />}
      <div className="table-responsive">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee Id</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {technicians && technicians.map((technician) => (
              <tr key={technician.employee_id}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
                <td>
                  <button className="btn btn-outline-dark" onClick={() => handleDelete(technician.employee_id)}>
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default TechnicianList;
