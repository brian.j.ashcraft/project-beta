import React, { useState, useEffect } from 'react';


function ServiceHistory () {
    const [appts, setAppts] = useState([])
    const [search, setSearch] = useState('');
    const [results, setResults] = useState([])

    const handleInputChange = (e) => {
        setSearch(e.target.value);
    };

    const getData = async () => {
        let response = await fetch('http://localhost:8080/api/appointments/')
        let data = await response.json()
        setAppts(data.appts)
        setResults(data.appts)
    }

    useEffect(() => {
        getData()
    }, [])


    return (
        <>
        <h1 className="display-4 fw-bold text-center" style={{ color: "#0047ab", fontFamily: "Playfair Display, serif" }}>Appointment History</h1>
        <form>
            <input className="form-control" value={search} onChange={handleInputChange} placeholder="Search VIN Number"/>
            <button>Search VIN</button>
        </form>
      <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Vip</th>
                    <th>customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Statue</th>
                </tr>
            </thead>
            <tbody>
                {results?.map(appt => {
                    return(
                        <tr key={appt.id}>
                            <td>{appt.vin}</td>
                            <td>{appt.customer}</td>
                            <td>{appt.date}</td>
                            <td>{appt.time}</td>
                            <td>{appt.technician.name}</td>
                            <td>{appt.reason}</td>
                            <td>{appt.status}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
}

export default ServiceHistory;
