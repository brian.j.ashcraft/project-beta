# CarCar

Team:

* Person 1 - Which microservice?
Brian Ashcraft / Automobile Sales

* Person 2 - Which microservice?
Ashur Prather / Automobile Service

## Design

## Service microservice

In the Service microservice, I will handle the automobile service, appointments, and related resources. I will integrate the Service with the Inventory microservice. The goal is to insure accurate inventory management for service appointments.

## Service Models

The Service microservice will have these models:

- Service Appointment: Represents an appointment for automobile service. It will include attributes such as appointment date, vehicle information, customer details, and technician assignment.

- Technician: Represents a technician responsible for performing service tasks. It will include attributes such as name, experience level, and specializations.

- AutomobileVO: model containing vin and sold fields.

## Service Integration
The Service microservice will integrate with the Inventory microservice to access and update vehicle information for service appointments. This integration will ensure that service appointments are linked to the correct inventory items and that inventory availability is properly managed.

## Sales microservice

My service works with customer, sales, and salesperson models. Each have a "GET" "POST" "DELETE" so that a user can submit new data, view a list of existing data, or delete data for each model in the database. There is an automobileVO model as well that is used to help associate models data together such as a sale using a vin.
