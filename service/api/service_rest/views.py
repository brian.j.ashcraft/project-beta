import json
from django.http import JsonResponse
from .models import Technician, AutomobileVO, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        technicians_data = [technician.to_dict() for technician in technicians]
        return JsonResponse(
            {"technicians": technicians_data}, encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician.to_dict(), encoder=TechnicianEncoder, safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician invalid"}, status=404)


@require_http_methods(["GET", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician.to_dict(), encoder=TechnicianEncoder, safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician invalid"}, status=404)
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        filters = {}
        show_status = request.GET.get("status")
        show_vin = request.GET.get("vin")
        if show_status:
            filters["status"] = show_status
        if show_vin:
            filters["vin"] = show_vin
        appointments = Appointment.objects.filter(**filters)
        serialized_appointments = list(appointments.values())

        return JsonResponse(
            {"appointments": serialized_appointments}, status=200, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician invalid"}, status=400)
        try:
            vin = content["vin"]
            AutomobileVO.objects.get(vin=vin)
            content["vip"] = True
        except AutomobileVO.DoesNotExist:
            content["vip"] = False

        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            serialized_appointment = appointment.to_dict()
            serialized_appointment["technician"] = appointment.technician.to_dict()
            return JsonResponse(serialized_appointment, status=200, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment invalid"}, status=400)
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            props = ["date", "time", "status"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment invalid"}, status=400)
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
