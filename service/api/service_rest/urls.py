from django.urls import path
from .views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_appointment,
)


urlpatterns = [
    path("technicians/", api_technicians, name="technicians"),
    path("technicians/<int:pk>/", api_technician, name="technician"),
    path("appointments/", api_appointments, name="appointments"),
    path("appointments/<int:pk>/", api_appointment, name="appointment"),
]
